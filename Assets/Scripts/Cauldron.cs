using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;

[RequireComponent(typeof(CauldronSounds))]
public class Cauldron : MonoBehaviour
{
    public ButtonPrompter Prompter;

    public CauldronSounds Sounds;
    
    public Potion DimensionJuicePotion;

    public RecipeList RecipeList;

    public int InstabilityNeededToExplode;
    public float CooldownTimeAfterExplosion;

    public Animator Animator;
    public SpriteRenderer SpriteRenderer;

    private int _IngredientsAdded = 0;
    private int _IngredientsFinished = 0;
    private Dictionary<Ingredient.IngredientType, int> _FinishedIngredients = new Dictionary<Ingredient.IngredientType, int>();

    private int _Instability;

    private bool _PlayerInRange = false;

    private Tween _ActiveCooldownTween;
    private Sequence _CooldownRedFlashSequence;
    private float _Cooldown;

    private bool _OnCooldown = false;

    public bool OnCooldown => _OnCooldown;

    public float NormalizedCooldownProgress => _Cooldown;


    private Color _SpriteRendererTintAtStart;
    public bool CanCookIngredient => _OnCooldown == false;

    private void Start()
    {
        Sounds.StartFireCracklingLoop();
        _SpriteRendererTintAtStart = SpriteRenderer.color;
        ShakeFromInstability();
    }
    
    private void ShakeFromInstability()
    {
        SpriteRenderer.transform.DOShakePosition(0.2f, _Instability*0.01f, fadeOut: false)
            .OnComplete(ShakeFromInstability);
    }
    public void CookIngredient(Ingredient ingredient)
    {
        Sounds.StartCauldronBrewingLoop();
        _IngredientsAdded++;
        Sounds.PlayTossInLiquid();
        _Instability += ingredient.Instability;
        Animator.SetInteger("Instability", _Instability);
        StartCooldown(0, 1, ingredient.CookTime, ()=> ResolveIngredientFinishedCooking(ingredient));
    }

    public void PlayerEnteredRange()
    {
        _PlayerInRange = true;
    }

    public void PlayerLeftRange()
    {
        _PlayerInRange = false;
        Prompter.HideImage();
    }

    public bool CanBeCollectedFrom
    {
        get
        {
            if (_IngredientsFinished == 0)
            {
                return false;
            }
            
            return _IngredientsAdded == _IngredientsFinished;
        }  
    } 

    public Potion CollectPotion()
    {
        Potion potion = GetHighestPriorityCompletedRecipe().Potion;
        _FinishedIngredients.Clear();
        _IngredientsAdded = 0;
        _IngredientsFinished = 0;
        _Instability = 0;
        Sounds.StopCauldronBrewingLoop();
        Sounds.PlayFillAndCork();
        Animator.SetInteger("Instability", _Instability);
        return potion;
    }

    public Potion GetPotionThatWouldBeProduced() => GetHighestPriorityCompletedRecipe().Potion;

    private void OnTriggerEnter2D(Collider2D other)
    {
        Attack attack = other.GetComponent<Attack>();
        if (attack != null)
        {
            if (attack.OwnerSide == Attack.Side.Enemy && (Animator.GetBool("HasExploded") == false))
            {
                _Instability += attack.Damage;
                Destroy(attack.gameObject);
                Animator.SetInteger("Instability", _Instability);
                if(_Instability >= InstabilityNeededToExplode)
                {
                    Explode();
                }
                else
                {
                    PlayDamaged();
                } 
            }
        }
    }

    private void ResolveIngredientFinishedCooking(Ingredient ingredient)
    {
        if(_Instability >= InstabilityNeededToExplode)
        {
            Explode();
            return;
        }

        Ingredient.IngredientType type = ingredient.Type;
        int value = ingredient.Value;
        if(_FinishedIngredients.ContainsKey(type))
        {
            value += _FinishedIngredients[type];
        }
        _FinishedIngredients[type] = value;
        _IngredientsFinished++;
    }

    private void StartCooldown(float startValue, float targetValue, float duration, System.Action onComplete = null)
    {
        if(OnCooldown)
        {
            return;
        }

        _OnCooldown = true;
        _Cooldown = startValue;
        _ActiveCooldownTween = DOTween.To(()=> _Cooldown, x=> _Cooldown = x, targetValue, duration);
        _ActiveCooldownTween.onComplete += () => _OnCooldown = false;
        _ActiveCooldownTween.onComplete += () => onComplete();
        _ActiveCooldownTween.onComplete += () => { if(_CooldownRedFlashSequence != null) _CooldownRedFlashSequence.Kill(); };
    }

    private Recipe GetHighestPriorityCompletedRecipe()
    {
        Recipe highestPriorityRecipe = new Recipe() {Potion = DimensionJuicePotion, Priority = 0};

        if(_FinishedIngredients.Keys.Count > 1)
        {
            return highestPriorityRecipe;
        }

        foreach(Recipe recipe in RecipeList.Recipes)
        {
            Ingredient.IngredientType type = recipe.Type;
            bool canBeCompleted = _FinishedIngredients.ContainsKey(type) ? _FinishedIngredients[type] >= recipe.RequiredValue : false;
            bool priorityIsHigher = recipe.Priority > highestPriorityRecipe.Priority;
            if(canBeCompleted && priorityIsHigher)
            {
                highestPriorityRecipe = recipe;
            }
    
        }

        return highestPriorityRecipe;
    }

    private void Update()
    {
        if(_PlayerInRange)
        {
            if(CanBeCollectedFrom || CanCookIngredient)
            {
                Prompter.ShowImage();
            }
            else
            {
                Prompter.HideImage();
            }
        }
    }

    private void Explode()
    {
        _Instability = 0;
        _IngredientsAdded = 0;
        _IngredientsFinished = 0;
        _FinishedIngredients.Clear();
        Sounds.PlayCauldronExplosion();
        Animator.SetInteger("Instability", _Instability);
        Animator.SetBool("HasExploded", true);
        _CooldownRedFlashSequence = DOTween.Sequence()
            .Append(SpriteRenderer.DOColor(Color.red, 0.5f))
            .Append(SpriteRenderer.DOColor(_SpriteRendererTintAtStart, 0.5f))
            .SetLoops(-1)
            .Play();
        StartCooldown(1, 0, CooldownTimeAfterExplosion, () => Animator.SetBool("HasExploded", false));
    }

    private void PlayDamaged()
    {
        //Play an animation
        Sounds.PlayCauldronHit();
        DOTween.Sequence()
            .Append(SpriteRenderer.DOColor(Color.red, 0.20f))
            .Append(SpriteRenderer.DOColor(_SpriteRendererTintAtStart, 0.20f))
            .Play();

    }
}