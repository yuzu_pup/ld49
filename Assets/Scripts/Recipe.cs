[System.Serializable]
public class Recipe
{
    public Potion Potion;

    public Ingredient.IngredientType Type;
    public int RequiredValue;

    public int Priority;
}
