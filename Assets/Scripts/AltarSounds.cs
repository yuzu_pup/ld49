using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine;

public class AltarSounds : MonoBehaviour
{
    public AudioClip PortalAppear;
    
    public AudioSource OneShotSource;
    
    public void PlayPortalAppear()
    {
        OneShotSource.PlayOneShot(PortalAppear);
    }
}
