using UnityEngine;

[System.Serializable]
public class Ingredient
{
    public enum IngredientType
    {
        Fire,
        Healing,
        Dimensional,
        RapidFire
    }

    public string Name;
    public IngredientType Type;
    public int Value;
    public int CookTime;
    public int Instability;
    public Sprite InInventorySprite;
}
