using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    public enum Side
    {
        Player,
        Enemy
    }

    public Side OwnerSide;
    public int Damage;
}
