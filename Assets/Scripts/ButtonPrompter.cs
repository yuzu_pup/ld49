using UnityEngine;
using UnityEngine.UI;

public class ButtonPrompter : MonoBehaviour
{
    public Image Image;

    public Vector2 ScreenSpaceImageOffset;

    private bool _Showing;

    public void ShowImage()
    {
        if(_Showing)
        {
            return;
        }
        ButtonPromptManager.Instance.ShowPrompt(this);
        _Showing = true;
    }

    public void HideImage()
    {
        if(_Showing)
        {
            ButtonPromptManager.Instance.HidePrompt(this);
            _Showing = false;
        }
    }       
}
