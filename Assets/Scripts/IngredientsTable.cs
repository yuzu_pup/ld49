using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "IngredientsTable", menuName = "CauldronGame/IngredientsTable", order = 1)]
public class IngredientsTable : ScriptableObject
{
    [System.Serializable]
    public class IngredientsTableEntry
    {
        public IngredientPickup PickupPrefab;

        public int Weight;
    }

    public List<IngredientsTableEntry> Entries;

    public IngredientPickup GetRandomIngredientPickup()
    {
        int totalWeight = 1;
        foreach(IngredientsTableEntry entry in Entries)
        {
            totalWeight += entry.Weight;
        }

        int randomNumber = Random.Range(0, totalWeight);

        foreach (IngredientsTableEntry entry in Entries)
        {
            if (randomNumber <= entry.Weight)
            {
                return entry.PickupPrefab; 
            }
            randomNumber = randomNumber - entry.Weight;
        }

        return null;
    }
}
