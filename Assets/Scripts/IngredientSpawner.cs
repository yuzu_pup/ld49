using Pectin.Timers;
using UnityEngine;

public class IngredientSpawner : MonoBehaviour
{
    public float MinTimeBetweenSpawns;

    public float MaxTimeBetweenSpawns;

    public Transform SpawnVolume;

    public IngredientsTable IngredientsTable;

    void Start()
    {
        LoopingSpawnIngredient();
    }

    private void LoopingSpawnIngredient()
    {
        SpawnRandomIngredient();
        float timeToNextSpawn = Random.Range(MinTimeBetweenSpawns, MaxTimeBetweenSpawns);
        StartCoroutine(CoroutineTimer.Create(timeToNextSpawn, () => LoopingSpawnIngredient()));
    }

    private void SpawnRandomIngredient()
    {
        IngredientPickup pickupPrefab = IngredientsTable.GetRandomIngredientPickup();
        Vector2 positionInSpawnVolume = new Vector2(Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f));
        Vector2 spawnPosition = transform.TransformPoint(positionInSpawnVolume);
        IngredientPickup instance = Instantiate(pickupPrefab, spawnPosition, Quaternion.identity);
        instance.GetComponent<WorldTag>().World = WorldManager.Instance.CurrentWorld;
    }
}
