using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class KeyPromptAnimator : MonoBehaviour
{
    public SpriteRenderer SpeechBubbleSpriteRenderer;
    public SpriteRenderer KeySpirteRenderer;
    
    public GameObject KeyRoot;

    public float FadeInTime = 0.25f;
    public bool IsShowing { get; private set; }
    public bool Shake;

    private Sequence _ShowAnimation;
    private Sequence _HideAnimation;
    private Tween _ShiftShakeTween;

    public void Show()
    {
        KillAllAnimations();
        
        _ShowAnimation = DOTween.Sequence();
        _ShowAnimation.Append(SpeechBubbleSpriteRenderer.DOFade(1, FadeInTime)
            .From(0f));
        _ShowAnimation.Join(KeySpirteRenderer.DOFade(1, FadeInTime)
            .From(0f));
        _ShowAnimation.Play();

        if (Shake)
        {
            _ShiftShakeTween = KeyRoot.transform
                .DOShakePosition(0.1f, 0.01f, fadeOut: false)
                .SetLoops(-1);   
        }
        
        IsShowing = true;
    }

    public void Hide()
    {
        KillAllAnimations();
        
        _HideAnimation = DOTween.Sequence();
        _HideAnimation.Append(SpeechBubbleSpriteRenderer.DOFade(0f, FadeInTime)
            .From(1f));
        _HideAnimation.Join(KeySpirteRenderer.DOFade(0f, FadeInTime)
            .From(1f));
        _HideAnimation.AppendCallback(() =>
        {
            IsShowing = false;
            gameObject.SetActive(false);
        });
        _HideAnimation.Play();
    }

    private void KillAllAnimations()
    {
        _ShowAnimation?.Kill();
        _HideAnimation?.Kill();
        _ShiftShakeTween?.Kill();
    }
}