using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Pectin.Animation;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int StartingHP { get; private set; }
    public int HP;
    public bool StartDying;
    public bool Dying;

    public SpriteRenderer SpriteRenderer;
    public SpriteAnimationClip Idle;
    private SpriteAnimator _Animator;
    private GameObjectTargeter _Targeter;

    private Color _SpriteRendererTintAtStart;
    
    public event Action<int> TookDamage;
    public event Action Died;

    protected virtual void Awake()
    {
        StartingHP = HP;
        _Animator = GetComponent<SpriteAnimator>();
        _Targeter = GetComponent<GameObjectTargeter>();
    }

    protected virtual void Start()
    {
        _SpriteRendererTintAtStart = SpriteRenderer.color;
        if (Idle != null)
        {
            _Animator.Play(Idle);   
        }
    }

    protected void OnTriggerEnter2D(Collider2D other)
    {
        Attack p = other.gameObject.GetComponent<Attack>();
        if (p != null && p.OwnerSide != Attack.Side.Enemy)
        {
            HP -= p.Damage;
            if (HP <= 0)
            {
                StartDying = true;
            }
            else
            {
                DOTween.Sequence()
                .Append(SpriteRenderer.DOColor(Color.red, 0.10f))
                .Append(SpriteRenderer.DOColor(_SpriteRendererTintAtStart, 0.10f))
                .Play();
            }
            Destroy(p.gameObject);

            TookDamage?.Invoke(HP);
        }
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        _Animator.SetMirroring(_Targeter.DirectionToNearest.x > 0, false);

        if (Dying)
        {
            Died?.Invoke();
            Destroy(gameObject);
            return;
        }
        
        if (StartDying)
        {
            Dying = true;
            StartDying = false;
        }
    }
}
