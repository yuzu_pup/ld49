using System;
using System.Collections;
using System.Collections.Generic;
using Pectin;
using Pectin.Random.Extensions;
using Pectin.Timers;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyManager : MonoBehaviour
{
    [System.Serializable]
    public class EnemyTableEntry
    {
        public Enemy Enemy;
        public int Weight;
    }
    
    public List<EnemyTableEntry> EnemiesToSpawn = new List<EnemyTableEntry>();
    public Transform[] SpawnLocations;
    public float PeriodBetweenWaves;
    public bool SpawnEnemies;
    public Vector2Int NumberOfEnemiesToSpawnInWave;

    private void Start()
    {
        StartCoroutine(CoroutineTimer.CreateLooping(PeriodBetweenWaves, SpawnEnemyWave));
    }

    private bool SpawnEnemyWave()
    {
        int numberToSpawn =
            StaticApplicationContext.DefaultGlobalRandomNumberGenerator.ValueInRange(NumberOfEnemiesToSpawnInWave.x,
                NumberOfEnemiesToSpawnInWave.y + 1);

        for (int spawnNumber = 0; spawnNumber < numberToSpawn; spawnNumber++)
        {
            SpawnEnemyAtRandomLocation();
        }
        
        return SpawnEnemies;
    }

    private void SpawnEnemyAtRandomLocation()
    {
        Transform spawnLoc = SpawnLocations.GetRandomItem();
        Enemy enemyToSpawn = GetRandomEnemy();
        if (enemyToSpawn != null)
        {
            Instantiate(enemyToSpawn.gameObject, spawnLoc.position, Quaternion.identity);   
        }
    }
    
    public Enemy GetRandomEnemy()
    {
        int totalWeight = 1;
        foreach(EnemyTableEntry entry in EnemiesToSpawn)
        {
            totalWeight += entry.Weight;
        }

        int randomNumber = Random.Range(0, totalWeight);

        foreach (EnemyTableEntry entry in EnemiesToSpawn)
        {
            if (randomNumber <= entry.Weight)
            {
                return entry.Enemy; 
            }
            randomNumber = randomNumber - entry.Weight;
        }

        return null;
    }
}

