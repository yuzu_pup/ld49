using System;
using System.Collections;
using System.Collections.Generic;
using Pectin.Timers;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(GameObjectTargeter))]
public class MoveToNearestTarget : MonoBehaviour
{
    private GameObjectTargeter _Targeter;
    
    public float MoveSpeed = 3f;

    private void Awake()
    {
        _Targeter = GetComponent<GameObjectTargeter>();
    }

    private void Update()
    {
        if (_Targeter.HasTarget)
        {
            MoveToTarget();   
        }
    }

    private void MoveToTarget()
    {
        Vector2 delta = _Targeter.DirectionToNearest * MoveSpeed * Time.deltaTime;
        transform.Translate(delta, Space.World);
    }
}
