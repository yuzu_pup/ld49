using System;
using System.Collections;
using System.Collections.Generic;
using Pectin.Timers;
using UnityEngine;

[RequireComponent(typeof(GameObjectTargeter))]
public class AttackNearestTarget : MonoBehaviour
{
    public float AttackRange;
    public Attack AttackToUse;
    public float AttackCooldown;
    
    private bool _AttackIsOnCooldown;
    private GameObjectTargeter _Targeter;

    public event System.Action AttackPerformed;

    private void Awake()
    {
        _Targeter = GetComponent<GameObjectTargeter>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_Targeter.HasTarget)
        {
            if (CloseEnoughToTargetToAttack() && CanAttack())
            {
                AttackTarget();
                _AttackIsOnCooldown = true;
                StartCoroutine(CoroutineTimer.Create(AttackCooldown, () => { _AttackIsOnCooldown = false; }));
            }
        }
    }

    private bool CanAttack()
    {
        return !_AttackIsOnCooldown;
    }
    
    private bool CloseEnoughToTargetToAttack()
    {
        return _Targeter.DistanceToNearest < AttackRange;
    }
    
    private void AttackTarget()
    {
        Attack instance = GameObject.Instantiate(AttackToUse, 
            transform.position + (Vector3) _Targeter.DirectionToNearest, 
            Quaternion.identity);

        if (instance is Projectile p)
        {
            p.MovementDirection = _Targeter.DirectionToNearest;
        }
        
        AttackPerformed?.Invoke();
    }
}
