using System.Collections;
using Pectin.Random.Extensions;
using Pectin.Timers;
using UnityEngine;

public class AntagonistInvasionManager : MonoBehaviour
{
    public Transform[] SpawnPositions;
    public GameObject AntagonistPrefab;
    public float InvasionPeriod;
    public float InvasionDuration = 30f;
    
    public bool InvasionActive { get; private set; }
    
    private Antagonist _AntagonistInstance;
    private IEnumerator _ActiveNextInvasionTimer;
    private IEnumerator _ActiveEndInvasionTimer;
    
    // Start is called before the first frame update
    void Start()
    {
        StartInvasionTimer();
    }

    void Update()
    {
        if (InvasionActive)
        {
            if (_AntagonistInstance != null)
            {
                if (_AntagonistInstance.Dying)
                {
                    EndInvasion();       
                }
            }
        }
    }

    void StartInvasion()
    {
        if (!InvasionActive)
        {
            SpawnInvaderAtRandomLocation();
            StartInvasionEndTimer();
            InvasionActive = true;
        }
    }
    
    void EndInvasion()
    {
        if (_AntagonistInstance != null)
        {
            Destroy(_AntagonistInstance.gameObject);
        }
        InvasionActive = false;
        StartInvasionTimer();
    }

    void SpawnInvaderAtRandomLocation()
    {
        Vector3 spawnPos = SpawnPositions.GetRandomItem().position;
        GameObject go = Instantiate(AntagonistPrefab, spawnPos, Quaternion.identity);
        _AntagonistInstance = go.GetComponent<Antagonist>();
    }
    
    void StartInvasionTimer()
    {
        KillAllActiveTimers();
        _ActiveNextInvasionTimer = CoroutineTimer.Create(InvasionPeriod, StartInvasion); 
        StartCoroutine(_ActiveNextInvasionTimer);
    }

    void StartInvasionEndTimer()
    {
        KillAllActiveTimers();
        _ActiveNextInvasionTimer = CoroutineTimer.Create(InvasionPeriod, StartInvasion); 
        StartCoroutine(CoroutineTimer.Create(InvasionDuration, EndInvasion));
    }

    void KillAllActiveTimers()
    {
        if (_ActiveNextInvasionTimer != null)
        {
            StopCoroutine(_ActiveNextInvasionTimer);
        }
        
        if (_ActiveEndInvasionTimer != null)
        {
            StopCoroutine(_ActiveEndInvasionTimer);
        }
    }
}
