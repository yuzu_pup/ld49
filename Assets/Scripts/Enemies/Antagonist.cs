using System.Collections;
using System.Collections.Generic;
using Pectin.Random.Extensions;
using UnityEngine;

public class Antagonist : Enemy
{
    public bool IsInvader;
    public AntagonistSounds Sounds;

    private AttackNearestTarget _AttackNearestTarget;

    public float[] TimeBetweenLaughs;

    private IEnumerator _RandomLaughRoutine;

    protected override void Awake()
    {
        base.Awake();
        _AttackNearestTarget = GetComponent<AttackNearestTarget>();
        _AttackNearestTarget.AttackPerformed += () =>
        {
            Sounds.PlayEvilSpell();
        };

        TookDamage += (newHP) =>
        {
            Sounds.PlayEvilGrunt();
        };
    }

    protected override void Start()
    {
        base.Start();
        if (IsInvader)
        {
            Sounds.PlayEvilTeleportAndLaughGlobally();   
        }

        _RandomLaughRoutine = LaughWithRandomDelay();
        StartCoroutine(_RandomLaughRoutine);
    }

    private IEnumerator LaughWithRandomDelay()
    {
        while (true)
        {
            yield return new WaitForSeconds(TimeBetweenLaughs.GetRandomItem());
            Sounds.PlayEvilLaugh();
        }
    }
}
