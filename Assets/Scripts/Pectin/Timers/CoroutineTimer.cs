using System;
using System.Collections;
using UnityEngine;

namespace Pectin.Timers
{
    /// <summary>
    /// Creates coroutines for simple timers
    /// </summary>
    public class CoroutineTimer
    {
        public static IEnumerator Create(float delayInSeconds, Action action)
        {
            yield return new WaitForSeconds(delayInSeconds);
            action();
        }

        public static IEnumerator CreateLooping(float delayInSeconds, Func<bool> actionWhichReturnsTrueIfItShouldLoop)
        {
            yield return new WaitForSeconds(delayInSeconds);
            while(actionWhichReturnsTrueIfItShouldLoop()) { yield return new WaitForSeconds(delayInSeconds); }
        }
    }
}
