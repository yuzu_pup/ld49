﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pectin.Components
{
    public class EscapeToQuit : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            DontDestroyOnLoad(this.gameObject);
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
        }
    }
}
