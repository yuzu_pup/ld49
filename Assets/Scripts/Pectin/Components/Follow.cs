﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pectin.Components
{
    public class Follow : MonoBehaviour
    {
        [SerializeField]
        private Transform _Target;

        [SerializeField]
        private bool _FollowX;
        [SerializeField]
        private bool _FollowY;
        [SerializeField]
        private bool _FollowZ;

        // Update is called once per frame
        void Update()
        {
            Vector3 t = _Target.position;
            Vector3 myOld = transform.position;
            Vector3 followPos = new Vector3
            (
                _FollowX ? t.x : myOld.x,
                _FollowY ? t.y : myOld.y,
                _FollowZ ? t.z : myOld.z
            );
            transform.position = followPos;
        }
    }
}
