using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ICollectionExtensions
{
    public static bool IsEmpty(this ICollection collection) => collection.Count == 0;
}
