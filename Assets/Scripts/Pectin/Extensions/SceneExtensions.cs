
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class SceneExtensions
{
    public static List<T> GetAllComponentsOfType<T>(this Scene scene) where T : Component
    {
        List<T> foundComponents = new List<T>();
        GameObject[] roots = SceneManager.GetActiveScene().GetRootGameObjects();
        foreach (GameObject root in roots)
        {
            T[] components = root.GetComponentsInChildren<T>(true);
            foreach (T c in components)
            {
                foundComponents.Add(c);   
            }
        }

        return foundComponents;
    }
    
    public static List<GameObject> GetAllGameObjectsWithComponentOfType(this Scene scene, Type t)
    {
        List<GameObject> gameObjects = new List<GameObject>();
        GameObject[] roots = SceneManager.GetActiveScene().GetRootGameObjects();
        foreach (GameObject root in roots)
        {
            Component[] components = root.GetComponentsInChildren(t, true);
            foreach (Component c in components)
            {
                gameObjects.Add(c.gameObject);   
            }
        }

        return gameObjects;
    }
}
