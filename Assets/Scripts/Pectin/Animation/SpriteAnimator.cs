﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pectin.Animation
{
    public abstract class SpriteAnimator : MonoBehaviour
    {
        public event Action OnClipFinishedOneTime;
        
        protected SpriteAnimationClip Clip;
        protected float TimeElapsedInClip;

        private int _FrameShownLastUpdate;
        private int _FrameShownThisUpdate;

        private bool _LastMirrorX;
        private bool _LastMirrorY;
        
        
        public abstract void SetSprite(Sprite sprite);
        public abstract void SetMirroring(bool mirrorX, bool mirrorY);

        public void Play(SpriteAnimationClip clip)
        {
            Clip = clip;
            TimeElapsedInClip = 0;
            SetSprite(Clip.Sprites[0]);
            if (_LastMirrorX != clip.MirrorX || _LastMirrorY != clip.MirrorY)
            {
                SetMirroring(clip.MirrorX, clip.MirrorY);
            }

            _LastMirrorX = clip.MirrorX;
            _LastMirrorY = clip.MirrorY;
        }

        private void Start()
        {
            SetMirroring(false, false);
        }

        private void Update()
        {
            if (Clip != null)
            {
                TimeElapsedInClip += Time.deltaTime;

                _FrameShownThisUpdate = GetFrameIndexBasedOnClipBehavior();
                SetSprite(Clip.Sprites[_FrameShownThisUpdate]);
                if (ClipIsFinished())
                {
                    OnClipFinishedOneTime?.Invoke();
                    OnClipFinishedOneTime = null;
                }
            }

        }

        private int GetFrameIndexBasedOnClipBehavior()
        {
            switch (Clip.EndBehavior)
            {
                case SpriteAnimationClip.EndOfClipBehavior.Hold:
                    return Mathf.Min((int) (TimeElapsedInClip * Clip.FPS), Clip.Frames - 1) ;
                case SpriteAnimationClip.EndOfClipBehavior.Loop:
                default:
                    return (int) (TimeElapsedInClip * Clip.FPS) % Clip.Frames;
            }
        }

        private bool ClipIsFinished()
        {
            if (Clip.EndBehavior == SpriteAnimationClip.EndOfClipBehavior.Hold)
            {
                return _FrameShownLastUpdate == _FrameShownThisUpdate && _FrameShownThisUpdate == Clip.Frames - 1;
            }
            return false;
        }
    }
}
