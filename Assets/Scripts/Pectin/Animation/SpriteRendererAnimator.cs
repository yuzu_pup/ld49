﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pectin.Animation
{
    public class SpriteRendererAnimator : SpriteAnimator
    {
        public SpriteRenderer Renderer;
        
        public override void SetSprite(Sprite sprite)
        {
            Renderer.sprite = sprite;
        }

        public override void SetMirroring(bool mirrorX, bool mirrorY)
        {
            Renderer.flipX = mirrorX;
            Renderer.flipY = mirrorY;
        }
    }
}
