﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pectin.Animation
{
    public class ImageAnimator : SpriteAnimator
    {
        public Image Image;

        public override void SetSprite(Sprite sprite)
        {
            Image.sprite = sprite;
        }

        public override void SetMirroring(bool mirrorX, bool mirrorY)
        {
            throw new System.NotImplementedException();
        }
    }
}
