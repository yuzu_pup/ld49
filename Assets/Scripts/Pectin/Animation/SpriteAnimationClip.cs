﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pectin.Animation
{
    [CreateAssetMenu(menuName = "Pectin/Sprite Animation Clip")]
    public class SpriteAnimationClip : ScriptableObject
    {
        public enum EndOfClipBehavior
        {
            Hold,
            Loop
        }
        
        public Sprite[] Sprites;
        public int Frames => Sprites.Length;
        public float FPS;
        public EndOfClipBehavior EndBehavior;
        public bool MirrorX;
        public bool MirrorY;
    }
}
