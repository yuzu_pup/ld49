using System;
using System.Collections;
using System.Collections.Generic;
using Pectin.ObjectPooling;
using UnityEngine;

namespace Pectin.ObjectPooling
{
    public class GameObjectPool : ObjectPool<GameObject>
    {
        public GameObjectPool(GameObject prefab) : base(() => UnityEngine.Object.Instantiate(prefab))
        {
            
        }
    }
}
