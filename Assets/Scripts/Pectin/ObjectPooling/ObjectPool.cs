﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Pectin.ObjectPooling
{
    public class ObjectPool<T> where T: class
    {
        private readonly Dictionary<T, bool> _ObjectsUsedMap = new Dictionary<T, bool>();
        protected Func<T> _Constructor;
        
        private int _NumberOfNewInstancesToAllocateOnGrow = 1;
        private int _ObjectsInUse;
        
        private const int _MAX_GROW_AMOUNT = 32;

        public ObjectPool(Func<T> constructor)
        {
            _Constructor = constructor;
        }

        public T Get(Action<T> initializer)
        {
            if (_ObjectsInUse == _ObjectsUsedMap.Count)
            {
                GrowPool();
            }

            T freeObject = _ObjectsUsedMap.First(kvp => kvp.Value == false).Key;
            _ObjectsUsedMap[freeObject] = true;
            _ObjectsInUse++;
            
            initializer?.Invoke(freeObject);
            return freeObject;
        }

        public void Return(T obj)
        {
            if (_ObjectsUsedMap.ContainsKey(obj) == false)
            {
                throw new ArgumentException("Tried to return object to pool that was not part of pool?");
            }
            
            _ObjectsInUse--;
            _ObjectsUsedMap[obj] = false;
        }

        private void GrowPool(bool inUse = false)
        {
            for (int ii = 0; ii < _NumberOfNewInstancesToAllocateOnGrow; ii++)
            {
                T inst = _Constructor();
                _ObjectsUsedMap.Add(inst, false);
            }

            _NumberOfNewInstancesToAllocateOnGrow = 
                Mathf.Min(_NumberOfNewInstancesToAllocateOnGrow * 2, _MAX_GROW_AMOUNT);
        }
    }
}

