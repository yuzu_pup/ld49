

namespace Pectin.Random
{
    public class UnityEngineRNG : RandomNumberGenerator
    {
        public float Value()
        {
            float selected = UnityEngine.Random.value;
            
            while (selected == 1.0f)
            {
                selected = UnityEngine.Random.value;
            }

            return selected;
        }

        public float ValueInRange(float minInclusive, float maxExclusive)
        {
            return Value() * (minInclusive - maxExclusive) + minInclusive;
        }
        
        public int ValueInRange(int minInclusive, int maxExclusive) => 
            UnityEngine.Random.Range(minInclusive, maxExclusive);
    }
}
