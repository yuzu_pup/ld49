using System;
using System.Collections.Generic;

namespace Pectin.Random.Extensions
{
    public static class RandomListExtensions
    {
        /// <param name="list"></param>
        /// <param name="rng"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns>A random item in this list</returns>
        /// <exception cref="InvalidOperationException">If the list is empty</exception>
        public static T GetRandomItem<T>(this IList<T> list, RandomNumberGenerator rng)
        {
            if (list.Count == 0)
            {
                throw new InvalidOperationException("List contained no items to select from");
            }

            if (list.Count == 1)
            {
                return list[0];
            }

            return list[rng.ValueInRange(0, list.Count)];
        }

        /// <param name="list"></param>
        /// <param name="numberOfItems">How many items to return</param>
        /// <param name="rng"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns>A random permutation of <paramref name="numberOfItems"/> items from this list</returns>
        /// <exception cref="ArgumentException">
        /// If <paramref name="numberOfItems"/> is negative or GTE the number of items in the list
        /// </exception>
        public static List<T> GetRandomPermutation<T>(this IList<T> list, int numberOfItems, RandomNumberGenerator rng)
        {
            if (numberOfItems <= 0 || numberOfItems >= list.Count)
            {
                throw new ArgumentException($"Invalid numbers of items for permutation: {numberOfItems}. Must be a number within the size of the list");
            }

            if (list.Count == 0)
            {
                return new List<T>();
            }
            
            List<T> copy = list.ShuffledCopy(rng);
            copy.RemoveRange(numberOfItems, list.Count - numberOfItems);
            return copy;
        }
        
        /// <param name="list"></param>
        /// <param name="numberOfItems">How many items to include in the list</param>
        /// <param name="rng"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns>A random combination of <paramref name="numberOfItems"/> items from this list</returns>
        /// <exception cref="ArgumentException">
        /// If <paramref name="numberOfItems"/> is negative or GTE the number of items in the list
        /// </exception>
        public static List<T> GetRandomCombination<T>(this IList<T> list, int numberOfItems, RandomNumberGenerator rng)
        {
            if (numberOfItems >= list.Count)
            {
                throw new ArgumentException($"Invalid numbers of items: {numberOfItems}. Must be greater than zero");
            }

            if (list.Count == 0)
            {
                return new List<T>();
            }
            
            List<T> randomItems = new List<T>(numberOfItems);
            for (int ii = 0; ii < numberOfItems; ii++)
            {
                randomItems.Add(list.GetRandomItem(rng));
            }

            return randomItems;
        }

        /// <summary>
        /// Shuffles the provided list in-place
        /// </summary>
        /// <param name="list"></param>
        /// <param name="rng"></param>
        /// <typeparam name="T"></typeparam>
        public static void Shuffle<T>(this IList<T> list, RandomNumberGenerator rng)
        { 
            if (list.Count <= 1)
            {
                return;
            }

            for (int ii = 0; ii < list.Count; ii++)
            {
                int jj = rng.ValueInRange(ii, list.Count);
                T tmp = list[ii];
                list[ii] = list[jj];
                list[jj] = tmp;
            }
        }

        /// <param name="list"></param>
        /// <param name="rng"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns>A shuffled copy of the provided list</returns>
        public static List<T> ShuffledCopy<T>(this IList<T> list, RandomNumberGenerator rng)
        {
            List<T> copy = new List<T>(list);
            copy.Shuffle(rng);
            return copy;
        }

        #region Convenience Signatures
        
        /// <param name="list"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns>A random item from the provided list</returns>
        /// <remarks>This uses the default global <see cref="RandomNumberGenerator"/></remarks>
        public static T GetRandomItem<T>(this IList<T> list) => 
            list.GetRandomItem(StaticApplicationContext.DefaultGlobalRandomNumberGenerator);
        
        /// <param name="list"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns>A random permutation of the items in this list</returns>
        /// <remarks>This uses the default global <see cref="RandomNumberGenerator"/></remarks>
        public static List<T> GetRandomPermutation<T>(this IList<T> list) => 
            list.GetRandomPermutation(list.Count, StaticApplicationContext.DefaultGlobalRandomNumberGenerator);
        
        /// <param name="list"></param>
        /// <param name="numberOfItems">How many items to include in the list</param>
        /// <typeparam name="T"></typeparam>
        /// <returns>A random permutation of <paramref name="numberOfItems"/> items from this list</returns>
        /// <exception cref="ArgumentException">
        /// If <paramref name="numberOfItems"/> is negative or GTE the number of items in the list
        /// </exception>
        /// <remarks>This uses the default global <see cref="RandomNumberGenerator"/></remarks>
        public static List<T> GetRandomPermutation<T>(this IList<T> list, int numberOfItems) =>
            list.GetRandomPermutation(numberOfItems, StaticApplicationContext.DefaultGlobalRandomNumberGenerator);
        
        /// <param name="list"></param>
        /// <param name="numberOfItems">How many items to include in the list</param>
        /// <typeparam name="T"></typeparam>
        /// <returns>A random combination of <paramref name="numberOfItems"/> items from this list</returns>
        /// <exception cref="ArgumentException">
        /// If <paramref name="numberOfItems"/> is negative or GTE the number of items in the list
        /// </exception>
        /// <remarks>This uses the default global <see cref="RandomNumberGenerator"/></remarks>
        public static List<T> GetRandomCombination<T>(this IList<T> list, int numberOfItems) =>
            list.GetRandomCombination(numberOfItems, StaticApplicationContext.DefaultGlobalRandomNumberGenerator);
        
        
        /// <summary>
        /// Shuffles the provided list in-place
        /// </summary>
        /// <param name="list"></param>
        /// <typeparam name="T"></typeparam>
        /// <remarks>This uses the default global <see cref="RandomNumberGenerator"/></remarks>
        public static void Shuffle<T>(this IList<T> list) =>
            list.Shuffle(StaticApplicationContext.DefaultGlobalRandomNumberGenerator);
        
        /// <param name="list"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns>A shuffled copy of the provided list</returns>
        /// <remarks>This uses the default global <see cref="RandomNumberGenerator"/></remarks>
        public static List<T> ShuffledCopy<T>(this IList<T> list) =>
            list.ShuffledCopy(StaticApplicationContext.DefaultGlobalRandomNumberGenerator);
        
        #endregion Convenience Signatures
    }
}
