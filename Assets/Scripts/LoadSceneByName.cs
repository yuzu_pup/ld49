using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneByName : MonoBehaviour
{
        public string SceneName;

        public void LoadScene()
        {
            SceneManager.LoadScene(SceneName);
        }
}
