using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RecipeList", menuName = "CauldronGame/RecipeList", order = 1)]
public class RecipeList : ScriptableObject
{
    public List<Recipe> Recipes;
}
