using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pickup : MonoBehaviour
{
    public float PickupRadius;
    protected bool _WasCollected = false;
    public SpriteRenderer SpriteRenderer;
    
    public bool CanBeCollected
    {
        get
        {
            bool withinRadius = GetSquaredDistanceToPlayer() < PickupRadius * PickupRadius;
            return withinRadius && (_WasCollected == false);
        }
    }

    public virtual object PickUp()
    {
        _WasCollected = true;
        return null;
    }

    private float GetSquaredDistanceToPlayer()
    {
        PlayerController playerController = GameObject.FindObjectOfType<PlayerController>();
        Rigidbody2D body = playerController.body;
        return (body.transform.position - transform.position).sqrMagnitude;
    }
}
