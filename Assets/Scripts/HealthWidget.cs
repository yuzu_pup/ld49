using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class HealthWidget : MonoBehaviour
{
    public Sprite EmptyHeart;
    public Sprite FilledHeart;

    public GameObject HeartContainerObject;
    public List<Image> Hearts = new List<Image>();

    private int _LastSeenHealth;

    void Awake()
    {
        PlayerController playerController = GameObject.FindObjectOfType<PlayerController>();
        _LastSeenHealth = playerController.Health;
        playerController.HealthChanged += PlayerHealthChanged;
        UpdateHealthDisplay(playerController.Health);
    }

    private void PlayerHealthChanged(int newValue)
    {
        int delta = newValue - _LastSeenHealth;

        UpdateHealthDisplay(newValue);
        if (delta < 0)
        {
            foreach (Image heart in Hearts)
            {
                heart.DOColor(Color.red, 0.25f).OnComplete(() => heart.DOColor(Color.white, 0.25f));
                HeartContainerObject.transform.DOShakePosition(0.5f, 10f);
            }   
        }

        if (delta > 0)
        {
            foreach (Image heart in Hearts)
            {
                heart.DOColor(Color.green, 0.25f).OnComplete(() => heart.DOColor(Color.white, 0.25f));
            }
        }

        _LastSeenHealth = newValue;
    }
    
    private void UpdateHealthDisplay(int health)
    {
        for(int heartIndex = 0; heartIndex < Hearts.Count; heartIndex++)
        {
            Hearts[heartIndex].sprite = heartIndex < health ? FilledHeart : EmptyHeart;
        }
    }
}
