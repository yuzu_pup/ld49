using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameObjectTargeter : MonoBehaviour
{
    public enum PossibleTarget
    {
        Cauldrons,
        Player
    }

    public PossibleTarget TargetType;
    
    public System.Type TargetSystemType;

    public bool HasTarget => Target != null;
    
    public GameObject Target { get; private set; }
    public Vector3 VectorToNearest { get; private set; }
    public float DistanceToNearest { get; private set; }
    public Vector3 DirectionToNearest { get; private set; }

    private List<GameObject> _Targets = new List<GameObject>();
    
    // Start is called before the first frame update
    void Start()
    {
        TargetSystemType = TargetType switch
        {
            PossibleTarget.Cauldrons => typeof(Cauldron),
            PossibleTarget.Player => typeof(PlayerController),
            _ => TargetSystemType = typeof(Cauldron)
        };

        _Targets = SceneManager.GetActiveScene().GetAllGameObjectsWithComponentOfType(TargetSystemType);
    }

    private void Update()
    {
        UpdateNearest();
    }

    private void UpdateNearest()
    {
        float closestSqrDist = float.MaxValue;
        foreach (GameObject t in _Targets)
        {
            Vector2 fromMeToTarget = t.transform.position - transform.position;
            float sqrDist = fromMeToTarget.sqrMagnitude;
            if (sqrDist < closestSqrDist)
            {
                closestSqrDist = sqrDist;
                Target = t;
                VectorToNearest = fromMeToTarget;
                DistanceToNearest = fromMeToTarget.magnitude;
                DirectionToNearest = fromMeToTarget.normalized;
            }
        }
    }
}
