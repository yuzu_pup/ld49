using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

public class InventoryWidget : MonoBehaviour
{
    public List<PotionRenderer> _PotionSlots = new List<PotionRenderer>();

    void Awake()
    {
        foreach(PotionRenderer renderer in _PotionSlots)
        {
            renderer.gameObject.SetActive(false);
        }

        Inventory inventory = GameObject.FindObjectOfType<Inventory>();
        if(inventory)
        {
            inventory.PotionWasAdded += DisplayPotions;
            inventory.PotionWasRemoved += RemovePotions;
        }
    }

    public void DisplayPotions(List<Potion> potions)
    {
        int slotIndex = 0;
        foreach (Potion potion in potions)
        {
            PotionRenderer renderer = _PotionSlots[slotIndex];
            renderer.gameObject.SetActive(true);
            renderer.RenderPotion(potion);
            if (slotIndex == potions.Count - 1)
            {
                renderer.transform.DOLocalMoveY(-40, 0.1f).From(0);
                renderer.transform.DOLocalMoveY(20, 2).SetEase(Ease.OutElastic);
            }
            if (slotIndex >= _PotionSlots.Count)
            {
                break;
            }
            slotIndex++;
        }

        if (slotIndex < _PotionSlots.Count)
        {
            PotionRenderer renderer = _PotionSlots[slotIndex++];
            renderer.gameObject.SetActive(false);
        }
    }
        public void RemovePotions(List<Potion> potions)
        {
            int slotIndex = 0;
            foreach (Potion potion in potions)
            {
                PotionRenderer renderer = _PotionSlots[slotIndex];
                renderer.gameObject.SetActive(true);
                renderer.RenderPotion(potion);

                if (slotIndex >= _PotionSlots.Count)
                {
                    break;
                }
                slotIndex++;
            }

            if (slotIndex < _PotionSlots.Count)
            {
                PotionRenderer renderer = _PotionSlots[slotIndex++];
                renderer.gameObject.SetActive(false);
            }
        }
    }

