using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TimeLeftInDarkWorldWidget : MonoBehaviour
{
    public CanvasGroup Group;
    public Slider TimeLeftBar;

    private Antagonist _Boss;

    private void Start()
    {
        Group.alpha = 0f;
        TimeLeftBar.value = 1f;
        WorldManager.Instance.PercentDimensionJuiceLeftChanged += AnimateTimeBarToNewValue;
        WorldManager.Instance.EnteredLightWorld += Hide;
        WorldManager.Instance.EnteredDarkWorld += Show;
    }

    private void AnimateTimeBarToNewValue(float percentLeft)
    {
        TimeLeftBar.value = percentLeft;
    }

    public void Show()
    {
        Group.DOFade(1, 0.5f).From(0f);
    }

    public void Hide()
    {
        Group.DOFade(0f, 0.5f).From(1);   
    }
}
