using UnityEngine;

public class CauldronSounds : MonoBehaviour
{
    public AudioClip CauldronBrewingLoop;
    public AudioClip CauldronHit;
    public AudioClip CauldronExplosion;
    public AudioClip FireCrackling;
    public AudioClip FillAndCork;
    public AudioClip TossInLiquid;

    public AudioSource FireCracklingSource;
    public AudioSource CauldronBrewingSource;
    public AudioSource OneShotSource;

    private void Start()
    {
        FireCracklingSource.loop = true;
        FireCracklingSource.clip = FireCrackling;
        
        CauldronBrewingSource.loop = true;
        CauldronBrewingSource.clip = CauldronBrewingLoop;
    }

    public void StartFireCracklingLoop()
    {
        if(FireCracklingSource.isPlaying == false)
            FireCracklingSource.Play();
    }

    public void StopFireCracklingLoop()
    {
        FireCracklingSource.Stop();
    }
    
    public void StartCauldronBrewingLoop()
    {
        if(CauldronBrewingSource.isPlaying == false)
            CauldronBrewingSource.Play();
    }

    public void StopCauldronBrewingLoop()
    {
        CauldronBrewingSource.Stop();
    }

    public void PlayCauldronHit()
    {
        OneShotSource.PlayOneShot(CauldronHit);
    }
    
    public void PlayCauldronExplosion()
    {
        OneShotSource.PlayOneShot(CauldronExplosion);
    }

    public void PlayFillAndCork()
    {
        OneShotSource.PlayOneShot(FillAndCork);
    }
    
    public void PlayTossInLiquid()
    {
        OneShotSource.PlayOneShot(TossInLiquid);
    }
}
