using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlayerSounds : MonoBehaviour
{
    public static PlayerSounds Instance;

    public AudioSource Source;
    
    public AudioClip FireballCast;
    public AudioClip PlayerTeleport;
    public AudioClip PortalBack;
    public AudioClip PowerUp;
    public AudioClip SmallAttack;

    private void Awake()
    {
        Instance = this;
    }

    private void Reset()
    {
        Source = GetComponent<AudioSource>();
    }

    public void PlayFireballCast()
    {
        Source.PlayOneShot(FireballCast);
    }

    public void PlayPlayerTeleport()
    {
        Source.PlayOneShot(PlayerTeleport);
    }
    
    public void PlayPortalBack()
    {
        Source.PlayOneShot(PortalBack);
    }
    
    public void PlayPowerUp()
    {
        Source.PlayOneShot(PowerUp);
    }

    public void PlaySmallAttack()
    {
        Source.PlayOneShot(SmallAttack);
    }
}
