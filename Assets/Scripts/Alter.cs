using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alter : MonoBehaviour
{
    public bool AlterReady { get; private set; }
    public float minSecondsOfJuiceNeededToPortal = 20f;

    public SpriteRenderer BGSpriteRenderer;
    public Sprite InactiveSprite;
    public Sprite ActiveSprite;
    public PortalAnimator PortalGameObject;
    public DJuicePromptAnimator DJuicePrompt;
    public KeyPromptAnimator ShiftKeyPrompt;

    private AltarSounds _AltarSounds;
    
    private bool _PlayerInRange = false;

    private void Awake()
    {
        _AltarSounds = GetComponent<AltarSounds>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
        DJuicePrompt.gameObject.SetActive(false);
        ShiftKeyPrompt.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (WorldManager.Instance.CurrentWorld == WorldLocation.Light)
        {
            AlterReady = WorldManager.Instance.SecondsOfDimensionJuiceRemaining 
                >= minSecondsOfJuiceNeededToPortal;
        }
        else
        {
            AlterReady = true;
        }

        UpdatePrompt();
    }
    
    private void UpdatePrompt()
    {
        BGSpriteRenderer.sprite = AlterReady ? ActiveSprite : InactiveSprite;
        if (PortalGameObject.gameObject.activeSelf != AlterReady)
        {
            if (AlterReady)
            {
                if (DJuicePrompt.IsShowing)
                {
                    DJuicePrompt.Hide();
                }

                if (ShiftKeyPrompt.IsShowing == false)
                {
                    ShiftKeyPrompt.gameObject.SetActive(true);
                    ShiftKeyPrompt.Show();   
                }
                _AltarSounds.PlayPortalAppear();
                PortalGameObject.gameObject.SetActive(true);    
            }
            else
            {
                if (DJuicePrompt.IsShowing == false && _PlayerInRange)
                {
                    DJuicePrompt.Show();
                }

                if (ShiftKeyPrompt.IsShowing)
                {
                    ShiftKeyPrompt.Hide();
                }
                PortalGameObject.Close();
            }
        }
    }

    public void OnPlayerEnteredRange()
    {
        if (AlterReady)
        {
            ShiftKeyPrompt.gameObject.SetActive(true);
            ShiftKeyPrompt.Show();
        }
        else
        {
            DJuicePrompt.gameObject.SetActive(true);
            DJuicePrompt.Show();   
        }
        _PlayerInRange = true;
    }
    public void OnPlayerExitedRange()
    {
        if(DJuicePrompt.IsShowing) DJuicePrompt.Hide();
        if(ShiftKeyPrompt.IsShowing) ShiftKeyPrompt.Hide();
        _PlayerInRange = false;
    }
    
    public void OnEnter()
    {
        WorldManager.Instance.SwitchWorld();
    }
    
    public void AddJuice(float juice)
    {
        WorldManager.Instance.SecondsOfDimensionJuiceRemaining  += juice;
    }

    //when player approaches alter, e to add dimension juice is shown and on button press, take players inventory of dimension juice and add it to alter, once alter reaches 50, open portal and e becomes e to shift.
    //
}
