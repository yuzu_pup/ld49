using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class DJuicePromptAnimator : MonoBehaviour
{
    public SpriteRenderer SpeechBubbleSpriteRenderer;
    public SpriteRenderer BottleBgSpriteRenderer;
    public SpriteRenderer BottleMgSpirteRenderer;
    
    public GameObject BottleRoot;

    public float FadeInTime = 0.25f;
    public float BottleRotationAngle = 15f;

    public bool IsShowing = false;

    private Sequence _ShowAnimation;
    private Sequence _HideAnimation;
    private Tween _BottleShakeTween;
    private Tween _BottleScaleTween;

    private Vector3 _InitialBottleScale;

    private void Start()
    {
        _InitialBottleScale = BottleRoot.transform.localScale;
    }

    public void Show()
    {
        KillAllAnimations();
        
        _ShowAnimation = DOTween.Sequence();
        _ShowAnimation.Append(SpeechBubbleSpriteRenderer.DOFade(1, FadeInTime)
            .From(0f));
        _ShowAnimation.Join(BottleBgSpriteRenderer.DOFade(1, FadeInTime)
            .From(0f));
        _ShowAnimation.Join(BottleMgSpirteRenderer.DOFade(1, FadeInTime)
            .From(0f));
        _ShowAnimation.Play();
        
        _BottleShakeTween = BottleRoot.transform
            .DORotate(new Vector3(0, 0, 15f), 1.0f)
                .From(new Vector3(0, 0, -15f))
                .SetEase((t, d, o, p) => t > 0.5 ? 1 : 0)
                .SetLoops(-1);
        _BottleScaleTween = BottleRoot.transform
            .DOScale(new Vector3(_InitialBottleScale.x * 1.15f, _InitialBottleScale.y * 1.2f, 1), 0.33f)
                .From(_InitialBottleScale)
                .SetLoops(-1, LoopType.Yoyo);
        IsShowing = true;
    }

    public void Hide()
    {
        KillAllAnimations();
        
        _HideAnimation = DOTween.Sequence();
        _HideAnimation.Append(SpeechBubbleSpriteRenderer.DOFade(0f, FadeInTime)
            .From(1f));
        _HideAnimation.Join(BottleBgSpriteRenderer.DOFade(0f, FadeInTime)
            .From(1f));
        _HideAnimation.Join(BottleMgSpirteRenderer.DOFade(0f, FadeInTime)
            .From(1f));
        _HideAnimation.AppendCallback(() =>
        {
            IsShowing = false;
            gameObject.SetActive(false);
        });
        _HideAnimation.Play();
    }

    private void KillAllAnimations()
    {
        _ShowAnimation?.Kill();
        _HideAnimation?.Kill();
        _BottleScaleTween?.Kill();
        _BottleShakeTween?.Kill();
    }
}
