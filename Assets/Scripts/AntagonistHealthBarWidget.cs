using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AntagonistHealthBarWidget : MonoBehaviour
{
    public CanvasGroup Group;
    public Slider HealthBar;

    private Antagonist _Boss;

    private void Start()
    {
        List<Antagonist> allAntagonists = SceneManager.GetActiveScene().GetAllComponentsOfType<Antagonist>();
        _Boss = allAntagonists.Find(antagonist =>
        {
            WorldTag wt = antagonist.GetComponent<WorldTag>();
            return wt.World == WorldLocation.Dark;
        });

        _Boss.TookDamage += AnimateHealthBarToNewValue;

        Group.alpha = 0f;
        HealthBar.value = 1f;
        
        WorldManager.Instance.EnteredLightWorld += Hide;
        WorldManager.Instance.EnteredDarkWorld += Show;
    }

    private void AnimateHealthBarToNewValue(int newHealth)
    {
        HealthBar.value = (float) newHealth / _Boss.StartingHP;
    }

    public void Show()
    {
        Group.DOFade(1, 0.5f).From(0f);
    }

    public void Hide()
    {
        Group.DOFade(0f, 0.5f).From(1);   
    }
}
