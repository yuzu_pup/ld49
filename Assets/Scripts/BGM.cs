using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGM : MonoBehaviour
{
    public AudioClip BGMTrack;
    public AudioClip BossBGMTrack;

    public AudioSource Source;

    public static BGM Instance;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;   
        }
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        PlayBGMTrackLooping();
    }

    public void PlayBGMTrackLooping()
    {
        Source.clip = BGMTrack;
        Source.loop = true;
        Source.Play();
    }
    
    public void PlayBossBGMTrackLooping()
    {
        Source.clip = BossBGMTrack;
        Source.loop = true;
        Source.Play();
    }

    public void StopBGMTrack()
    {
        Source.Stop();
    }
}
