using UnityEngine;
using UnityEngine.UI;

public class PotionRenderer: MonoBehaviour
{
    public Image BottleBackground;

    public Image BottleForeground;

    public Image Fill;

    public void RenderPotion(Potion potion)
    {
        BottleBackground.sprite = potion.BottleBackground;
        BottleForeground.sprite = potion.BottleForeground;
        Fill.sprite = potion.PotionFill;
    }

}
