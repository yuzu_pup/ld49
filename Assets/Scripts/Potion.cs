using UnityEngine;

[System.Serializable]
public class Potion
{
    public enum EffectType
    {
        Dimension,
        Fireball,
        GreaterFireball,
        Healing,
        RapidFire
    }

    public string Name;

    public Sprite BottleBackground;
    public Sprite BottleForeground;
    public Sprite PotionFill;

    public EffectType Type;
    public int Value;

    public override string ToString()
    {
        return $"Potion of {Type}";
    }
}
