using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class PortalAnimator : MonoBehaviour
{
    private Vector2 InitialXYScale;
    private Sequence _OpenSequence;
    private Sequence _CloseSequence;
    
    private void Awake()
    {
        InitialXYScale = new Vector2(transform.localScale.x, transform.localScale.y);
    }

    void OnEnable()
    {
        AnimateOpen();
    }

    public void Close()
    {
        AnimateClose();
    }

    private void AnimateOpen()
    {
        if (_CloseSequence != null && _CloseSequence.IsPlaying())
        {
            _CloseSequence.Kill();
        }
        
        _OpenSequence = DOTween.Sequence();
        _OpenSequence.Append(transform.DOScaleY(InitialXYScale.y, 0.25f)
            .From(0f)
            .SetEase(Ease.OutCubic));
        _OpenSequence.Append(transform.DOScaleX(InitialXYScale.x, 0.5f)
            .From(0f)
            .SetEase(Ease.OutCubic));
        _OpenSequence.Play();
    }

    private void AnimateClose()
    {
        if (_OpenSequence != null && _OpenSequence.IsPlaying())
        {
            _OpenSequence.Kill();
        }
        
        _CloseSequence = DOTween.Sequence(); 
        _CloseSequence.Append(transform.DOScaleY(0, 0.8f)
            .SetEase(Ease.Linear));
        _CloseSequence.Join(transform.DOScaleX(0, 0.8f)
            .SetEase(Ease.Linear));
        _CloseSequence.AppendCallback(() => { gameObject.SetActive(false); });
        _CloseSequence.Play();
    }
}
