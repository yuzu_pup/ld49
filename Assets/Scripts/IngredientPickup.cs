using DG.Tweening;
using UnityEngine;

public class IngredientPickup : Pickup
{
    public Ingredient Ingredient;

    public void InitializeFromIngredient(Ingredient ing)
    {
        Ingredient = ing;
        SpriteRenderer.sprite = ing.InInventorySprite;
    }
    
    private void Start()
    {
        SpriteRenderer.transform.DOLocalRotate(new Vector3(0,0, 25), 2).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutCubic).From(new Vector3(0, 0, -25));
        SpriteRenderer.transform.DOScale(new Vector3(0.9f, 1.1f, 0), 0.25f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutCubic);
    }

    public override object PickUp()
    {
        base.PickUp();
        return Ingredient;
    }
}