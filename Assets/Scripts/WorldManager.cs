using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WorldManager : MonoBehaviour
{
    public static WorldManager Instance;
    
    public WorldLocation CurrentWorld { get; private set; }

    public float SecondsOfDimensionJuiceRemaining;

    public event Action<float> PercentDimensionJuiceLeftChanged;
    public event Action EnteredLightWorld;
    public event Action EnteredDarkWorld;

    private float _SecondsOfDimensionJuiceWhenDarkWorldEntered;
    
    [SerializeField]
    private GameObject DarkVision;
    
    public void SwitchWorld()
    {
        ToggleWorldVariableValueAndTriggerEvents();
        ToggleStateOfAllTaggedGameObjects();
    }

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        BGM.Instance.PlayBGMTrackLooping();
    }

    private void Update()
    {
        if (CurrentWorld == WorldLocation.Dark)
        {
            SecondsOfDimensionJuiceRemaining =
                Mathf.Clamp(SecondsOfDimensionJuiceRemaining - Time.deltaTime, 0, float.MaxValue);

            PercentDimensionJuiceLeftChanged?.Invoke(
                SecondsOfDimensionJuiceRemaining / _SecondsOfDimensionJuiceWhenDarkWorldEntered);
            
            if (SecondsOfDimensionJuiceRemaining == 0)
            {
                SwitchWorld();   
            }
        }
    }

    private void ToggleStateOfAllTaggedGameObjects()
    {
        List<WorldTag> allTaggedObjects = SceneManager.GetActiveScene().GetAllComponentsOfType<WorldTag>();
        foreach (WorldTag taggedObject in allTaggedObjects)
        {
            taggedObject.gameObject.SetActive(taggedObject.World == CurrentWorld);
        }
    }
    
    private void ToggleWorldVariableValueAndTriggerEvents()
    {
        if (CurrentWorld == WorldLocation.Light)
        {
            CurrentWorld = WorldLocation.Dark;
            BGM.Instance.PlayBossBGMTrackLooping();
            _SecondsOfDimensionJuiceWhenDarkWorldEntered = SecondsOfDimensionJuiceRemaining;
            EnteredDarkWorld?.Invoke();
            DarkVision.gameObject.SetActive(CurrentWorld == WorldLocation.Dark);
            return;
        }
        
        if (CurrentWorld == WorldLocation.Dark)
        {
            CurrentWorld = WorldLocation.Light;
            BGM.Instance.PlayBGMTrackLooping();
            EnteredLightWorld?.Invoke();
            DarkVision.gameObject.SetActive(CurrentWorld == WorldLocation.Dark);
            return;
        }
    }
}
