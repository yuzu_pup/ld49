using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CauldronUIStatusManager : MonoBehaviour
{
    public Slider CooldownProgressBarPrefab;

    public Vector2 CooldownProgressBarOffset;

    public Canvas Canvas;

    private Dictionary<Cauldron, Slider> _ProgressBarsToCauldrons = new Dictionary<Cauldron, Slider>();

    void Awake()
    {
        Cauldron[] cauldronsInScene = GameObject.FindObjectsOfType<Cauldron>();

        foreach(Cauldron cauldron in cauldronsInScene)
        {
            Slider progressBar = Instantiate(CooldownProgressBarPrefab, Canvas.transform);
            progressBar.minValue = 0;
            progressBar.maxValue = 1;
            _ProgressBarsToCauldrons.Add(cauldron, progressBar);
        }
    }

    void Update()
    {
       foreach(Cauldron cauldron in _ProgressBarsToCauldrons.Keys)
        {
            Slider progressBar = _ProgressBarsToCauldrons[cauldron];
            if(cauldron.OnCooldown == false)
            {
                progressBar.gameObject.SetActive(false);
                continue;
            }
            progressBar.gameObject.SetActive(true);
            Vector2 screenPoint = Camera.main.WorldToScreenPoint(cauldron.transform.position);
            progressBar.transform.position = screenPoint + CooldownProgressBarOffset;
            progressBar.value = cauldron.NormalizedCooldownProgress;
        }
    }
}