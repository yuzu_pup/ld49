using DG.Tweening;
using UnityEngine;

public class PlayerHUD : MonoBehaviour
{
    public CanvasGroup DeathMenu;
    public CanvasGroup WinMenu;

    void Start()
    {
        WinMenu.gameObject.SetActive(false);
        DeathMenu.gameObject.SetActive(false);
        PlayerController playerController = GameObject.FindObjectOfType<PlayerController>();
        playerController.HasDied += OpenDeathMenu;
        playerController.HasWon += OpenWinMenu;
    }

    private void OpenDeathMenu()
    {
        DeathMenu.gameObject.SetActive(true);
        DeathMenu.alpha = 0;
        DeathMenu.DOFade(1, 1);
    }
    
    private void OpenWinMenu()
    {
        WinMenu.gameObject.SetActive(true);
        WinMenu.alpha = 0;
        WinMenu.DOFade(1, 1);
    }
}
