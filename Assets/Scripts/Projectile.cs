using System;
using System.Collections;
using System.Collections.Generic;
using Pectin.Timers;
using UnityEngine;

public class Projectile : Attack
{
    public Vector2 MovementDirection;
    public float Speed;
    public Vector2 SpriteForward = new Vector2(0, 1);
    public float Lifetime;

    private IEnumerator _LifetimeTimerCoroutine;
    
    void Start()
    {
        _LifetimeTimerCoroutine = CoroutineTimer.Create(Lifetime, () =>
        {
            Destroy(gameObject);
        });
        StartCoroutine(_LifetimeTimerCoroutine);
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.FromToRotation(SpriteForward, MovementDirection);
        Vector2 delta = Speed * MovementDirection * Time.deltaTime;
        transform.Translate(delta, Space.World);
    }

    private void OnDestroy()
    {
        if (_LifetimeTimerCoroutine != null)
        {    
            StopCoroutine(_LifetimeTimerCoroutine);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Wall")
        {
            Destroy(gameObject);
        }

    }
}
