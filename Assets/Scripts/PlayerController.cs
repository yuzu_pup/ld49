using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Pectin.Animation;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

[RequireComponent(typeof(Inventory))]
public class PlayerController : MonoBehaviour
{
    public Rigidbody2D body;
    public float MoveSpeed = 3.0f;
    //private bool idle = true;
    public int Health = 5;
    private Vector2 Crosshair;
    private bool ShotIsOnCooldown;
    [FormerlySerializedAs("rateOfFire")] public float BasicShotCooldownTime = 1f;
    public SpriteRenderer PlayerRenderer;
    public SpriteRenderer HeldIngredientRenderer;
    public bool Winner;

    public SpriteAnimationClip Idle;
    public SpriteAnimationClip IdleWithItem;
    public SpriteAnimationClip Walking;
    public SpriteAnimationClip WalkingWithItem;

    public Projectile Bullet;
    public Projectile Fireball;
    public Projectile GreaterFireball;

    public GameObject IngredientPickupPrefab;

    private Inventory _Inventory;
    private Pickup _PickupPlayerIsInRangeOf;
    private Cauldron _CauldronPlayerIsInRangeOf;
    private Alter _AlterPlayerIsInRangeOf;
    private Vector2 _DirectionToMouse;
    private Vector2 _MovementFromInput;
    private Vector2 _VelocityLastFrame;
    private Vector2 _VelocityThisFrame;
    private bool _HoldingIngredientLastFrame;
    private SpriteAnimator _Animator;
    private Sequence _TookDamageRedFlashSequence;
    private Color _SpriteRendererTintAtStart;

    private int _MaxHealth;

    private bool HoldingAnIngredient => _Inventory.Ingredients.Count > 0;
    private bool CanShootBasicShot => ShotIsOnCooldown == false;

    private bool CanUsePotion
    {
        get
        {
            return _Inventory.HasUsablePotion;
        }
    }

    public event Action<int> HealthChanged;

    public bool IsDead {get; private set;} = false;
    public event Action HasDied;
    public event Action HasWon;

    private void Awake()
    {
        _Inventory = GetComponent<Inventory>();
        _Animator = GetComponent<SpriteAnimator>();
        
        List<Antagonist> allAntagonists = SceneManager.GetActiveScene().GetAllComponentsOfType<Antagonist>();
        allAntagonists.Find(antagonist =>
        {
            WorldTag wt = antagonist.GetComponent<WorldTag>();
            return wt.World == WorldLocation.Dark;
        }).Died += () =>
        {
            Winner = true;
            HasWon?.Invoke();
        };

        _MaxHealth = 5;
    }

    // Start is called before the first frame update
    void Start()
    {
        WorldManager.Instance.EnteredLightWorld -= PlayerSounds.Instance.PlayPortalBack;
        WorldManager.Instance.EnteredLightWorld += PlayerSounds.Instance.PlayPortalBack;
        body = GetComponent<Rigidbody2D>();
        ShotIsOnCooldown = false;
        _Animator.Play(Idle);
        _SpriteRendererTintAtStart = PlayerRenderer.color;
    }

    void Update()
    {
        if (Winner || IsDead)
        {
            return;
        }
        
        UpdateHeldItemSprite();
        UpdateDirectionToMouse();
        HandleInput();
        UpdateAnimations();
        _VelocityLastFrame = _VelocityThisFrame;
        _VelocityThisFrame =  new Vector2(Input.GetAxis("Horizontal") * MoveSpeed, Input.GetAxis("Vertical") * MoveSpeed);
        _HoldingIngredientLastFrame = HoldingAnIngredient;
    }
    
    void FixedUpdate()
    {
        if (Winner || IsDead)
        {
            body.velocity = Vector2.zero;
            return;
        }

        body.velocity = _VelocityThisFrame;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Attack a = other.GetComponent<Attack>();
        if (a != null)
        {
            if (a.OwnerSide == Attack.Side.Enemy)
            {
                GetHitBy(a);
                Destroy(a.gameObject);   
            }
        }
        
        Cauldron cauldron = other.GetComponentInParent<Cauldron>();
        if (cauldron != null)
        {
            _CauldronPlayerIsInRangeOf = cauldron;
            _CauldronPlayerIsInRangeOf.PlayerEnteredRange();
        }

        Pickup pickup = other.GetComponent<Pickup>();
        if (pickup != null)
        {
            if (_Inventory.CanHold(pickup))
            {
                _PickupPlayerIsInRangeOf = pickup;
            }
        }
        Alter alter = other.GetComponentInParent<Alter>();
        if (alter != null)
        {
            Debug.Log($"Near: {alter}");
            _AlterPlayerIsInRangeOf = alter;
            _AlterPlayerIsInRangeOf.OnPlayerEnteredRange();
        }
    }
    
    private void OnTriggerExit2D(Collider2D other)
    {
        Pickup pickup = other.GetComponent<Pickup>();
        if (pickup == _PickupPlayerIsInRangeOf)
        {
            _PickupPlayerIsInRangeOf = null;
        }
        
        Cauldron cauldron = other.GetComponentInParent<Cauldron>();
        if (_CauldronPlayerIsInRangeOf != null && cauldron == _CauldronPlayerIsInRangeOf)
        {
            _CauldronPlayerIsInRangeOf.PlayerLeftRange();
            _CauldronPlayerIsInRangeOf = null;
        }
        Alter alter = other.GetComponentInParent<Alter>();
        if(_AlterPlayerIsInRangeOf != null && alter == _AlterPlayerIsInRangeOf)
        {
            Debug.Log($"Left: {alter}");
            _AlterPlayerIsInRangeOf.OnPlayerExitedRange();
            _AlterPlayerIsInRangeOf = null;
        }
    }
    
    void GetHitBy(Attack a)
    {
        Health -= a.Damage;
        HealthChanged?.Invoke(Health);
        _TookDamageRedFlashSequence = DOTween.Sequence()
            .Append(PlayerRenderer.DOColor(Color.red, 0.25f))
            .Append(PlayerRenderer.DOColor(_SpriteRendererTintAtStart, 0.25f))
            .Play();
        
        if (Health <= 0 && (IsDead == false))
        {
            IsDead = true;
            HasDied?.Invoke();
        }
    }
    
    void ShootProjectileAtMouse(Projectile projectile)
    {
        GameObject spell = Instantiate(projectile.gameObject, transform.position, Quaternion.identity);
        Projectile p = spell.GetComponent<Projectile>();
        p.MovementDirection = _DirectionToMouse;
    }
    
    IEnumerator CooldownShot()
    {
        yield return new WaitForSeconds(BasicShotCooldownTime);
        ShotIsOnCooldown = false;
    }

    private void UpdateHeldItemSprite()
    { 
        if (_Inventory.Ingredients.Count > 0)
        {
            HeldIngredientRenderer.sprite = _Inventory.Ingredients[0].InInventorySprite;
        }
        else
        {
            HeldIngredientRenderer.sprite = null;
        }
    }

    private void UpdateDirectionToMouse()
    {
        Crosshair = Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
        _DirectionToMouse = (Crosshair - (Vector2)transform.position).normalized;
    }

    private void HandleInput()
    {
        if (Input.GetButton("Fire1") && CanShootBasicShot)
        {
            ShootBasicShot();
        }
        
        if (Input.GetMouseButtonDown(1) && CanUsePotion)
        {
            UseFirstUsablePotion();
        }
        
        if (_CauldronPlayerIsInRangeOf != null)
        {
            HandleCauldronInput();
        }
        
        if (HoldingAnIngredient)
        {
            HandleHoldingIngredientInput();
        }
        else
        {
            if (_PickupPlayerIsInRangeOf != null)
            {
                HandlePickupInput();
            }   
        }

        if (_AlterPlayerIsInRangeOf != null)
        {
            HandleAlterInput();
        }
    }

    private void UpdateAnimations()
    {
        bool itemHoldingStateChanged = _HoldingIngredientLastFrame != HoldingAnIngredient;
        bool cameToAStop = _VelocityLastFrame.sqrMagnitude > 0 && Mathf.Approximately(_VelocityThisFrame.sqrMagnitude, 0f);
        bool startedMoving = Mathf.Approximately(_VelocityLastFrame.sqrMagnitude, 0f) && _VelocityThisFrame.sqrMagnitude > 0;
        bool wasMoving = _VelocityLastFrame.sqrMagnitude > 0 && _VelocityThisFrame.sqrMagnitude > 0;
        bool xChangedSignOrBecameGreaterThanZero = _VelocityLastFrame.x < 0 && _VelocityThisFrame.x >= 0
                            || _VelocityLastFrame.x >= 0 && _VelocityThisFrame.x < 0
                            || _VelocityLastFrame.x == 0 && _VelocityThisFrame.x > 0; 
        if(startedMoving || (wasMoving && itemHoldingStateChanged))
        {
            _Animator.Play(HoldingAnIngredient ? WalkingWithItem : Walking);
        }

        if (wasMoving == false && itemHoldingStateChanged)
        {
            _Animator.Play(HoldingAnIngredient ? IdleWithItem : Idle);
        }

        if (cameToAStop)
        {
            _Animator.Play(HoldingAnIngredient ? IdleWithItem : Idle);
        }

        if (startedMoving || (wasMoving && xChangedSignOrBecameGreaterThanZero))
        {
            _Animator.SetMirroring(_VelocityThisFrame.x > 0, false);   
        }
    }

    private void ShootBasicShot()
    {
        PlayerSounds.Instance.PlaySmallAttack();
        ShootProjectileAtMouse(Bullet);
        ShotIsOnCooldown = true;
        StartCoroutine(CooldownShot());
    }

    private void UseFirstUsablePotion()
    {
        Potion weaponPot = _Inventory.GetFirstUsablePotion();
        _Inventory.Remove(weaponPot);
        Projectile projectileToShoot = null;
        switch(weaponPot.Type)
        {
            case Potion.EffectType.Fireball:
                projectileToShoot = Fireball;
                PlayerSounds.Instance.PlayFireballCast();
                break;
            case Potion.EffectType.GreaterFireball:
                projectileToShoot = GreaterFireball;
                PlayerSounds.Instance.PlayFireballCast();
                break;
            case Potion.EffectType.Healing:
                Health = Math.Min(Health + weaponPot.Value, _MaxHealth);
                HealthChanged?.Invoke(Health);
                break;
            case Potion.EffectType.RapidFire:
                BasicShotCooldownTime *= 0.8f;
                PlayerSounds.Instance.PlayPowerUp();
                break;
        };

        if (projectileToShoot != null)
        {
            ShootProjectileAtMouse(projectileToShoot);
        }
    }

    private void HandleCauldronInput()
    {
        if (HoldingAnIngredient)
        {
            Ingredient firstIngredient = _Inventory.Ingredients[0];
            if (_CauldronPlayerIsInRangeOf.CanCookIngredient)
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    _CauldronPlayerIsInRangeOf.CookIngredient(firstIngredient);
                    _Inventory.Remove(firstIngredient);
                }
            }
        }
        else
        {
            if (_CauldronPlayerIsInRangeOf.CanBeCollectedFrom)
            {
                Potion potentialPotion = _CauldronPlayerIsInRangeOf.GetPotionThatWouldBeProduced();
                if (_Inventory.CanHold(potentialPotion))
                {
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        Potion p = _CauldronPlayerIsInRangeOf.CollectPotion();
                        _Inventory.PickUp(p);
                    }
                }
            }
        }
    }

    private void HandlePickupInput()
    {
        if (_Inventory.CanHold(_PickupPlayerIsInRangeOf))
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                _Inventory.PickUp(_PickupPlayerIsInRangeOf);
                Destroy(_PickupPlayerIsInRangeOf.gameObject);
                _PickupPlayerIsInRangeOf = null;
            }
        }
    }

    private void HandleAlterInput()
    {
        if (Input.GetKeyDown("left shift"))
        {
            PlayerSounds.Instance.PlayPlayerTeleport();
            _AlterPlayerIsInRangeOf.OnEnter();
        }

        if (_Inventory.HasAtLeastOneDimensionPotion)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                Potion dimensionPotion = _Inventory.GetFirstDimensionPotion();
                _Inventory.Remove(dimensionPotion);
                _AlterPlayerIsInRangeOf.AddJuice(dimensionPotion.Value);
            }   
        }
    }

    private void HandleHoldingIngredientInput()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Ingredient ing = _Inventory.Ingredients[0];
            _Inventory.Remove(ing);
            GameObject go = Instantiate(IngredientPickupPrefab, transform.position, Quaternion.identity);
            go.GetComponent<IngredientPickup>().InitializeFromIngredient(ing);
        }
    }
}
