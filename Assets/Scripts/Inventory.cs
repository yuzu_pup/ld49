using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public float MaxPotions;
    public float MaxHeldIngredients;
    public List<Potion> Potions = new List<Potion>();
    public List<Ingredient> Ingredients = new List<Ingredient>();

    public event Action<List<Ingredient>> IngredientWasAdded;
    public event Action<List<Ingredient>> IngredientWasRemoved;
    public event Action<List<Potion>> PotionWasAdded;
    public event Action<List<Potion>> PotionWasRemoved;

    public bool HasUsablePotion
    {
        get
        {
            return Potions.Find(pot => pot.Type != Potion.EffectType.Dimension) != null;
        }
    }

    public bool HasAtLeastOneDimensionPotion
    {
        get
        {
            return Potions.Find(pot => pot.Type == Potion.EffectType.Dimension) != null;
        }
    }

    public bool CanHold(object obj)
    {
        if (obj is Potion p)
        {
            return CanPickUpPotion(p);
        }

        if (obj is IngredientPickup i)
        {
            return CanPickUpIngredient(i);
        }

        return false;
    }
    
    public void PickUp(object obj)
    {
        if (obj is Potion p)
        {
            AddPotion(p);
            Debug.Log($"Added {p}");
        }

        if (obj is IngredientPickup ip)
        {
            Ingredient i = ip.Ingredient;
            AddIngredient(i);
            Debug.Log($"Added {i}");
        }

    }

    public void Remove(object obj)
    {
        if (obj is Potion p)
        {
            RemovePotion(p);
        }

        if (obj is Ingredient i)
        {
            RemoveIngredient(i);
        }
    }

    public Potion GetFirstUsablePotion()
    {
        return Potions.Find(pot => pot.Type != Potion.EffectType.Dimension);
    }

    public Potion GetFirstDimensionPotion()
    {
        return Potions.Find(pot => pot.Type == Potion.EffectType.Dimension);
    }

    private bool CanPickUpIngredient(IngredientPickup ingredient) => Ingredients.Count < MaxHeldIngredients;
    private bool CanPickUpPotion(Potion potion) => Potions.Count < MaxPotions;

    private void AddPotion(Potion pot)
    {
        Potions.Add(pot);
        PotionWasAdded?.Invoke(Potions);
    }

    private void AddIngredient(Ingredient i)
    {
        Ingredients.Add(i);
        IngredientWasAdded?.Invoke(Ingredients);
        
    }

    private void RemovePotion(Potion pot)
    {
        Potions.Remove(pot);
        PotionWasRemoved?.Invoke(Potions);
    }

    private void RemoveIngredient(Ingredient i)
    {
        Ingredients.Remove(i);
        IngredientWasRemoved?.Invoke(Ingredients);
    }
}
