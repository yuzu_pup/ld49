using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonPromptManager : MonoBehaviour
{
    public static ButtonPromptManager Instance;

    private Dictionary<ButtonPrompter, Image> _ImagesToActivePrompts = new Dictionary<ButtonPrompter, Image>();

    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        foreach(ButtonPrompter prompter in _ImagesToActivePrompts.Keys)
        {
            Image image = _ImagesToActivePrompts[prompter];
            Vector2 screenPoint = Camera.main.WorldToScreenPoint(prompter.transform.position);
            image.transform.position = screenPoint + prompter.ScreenSpaceImageOffset;
        }
    }

    public void ShowPrompt(ButtonPrompter prompt)
    {
        Image image = Instantiate(prompt.Image);
        image.transform.SetParent(transform);
        _ImagesToActivePrompts.Add(prompt, image);
    }

    public void HidePrompt(ButtonPrompter prompt)
    {
        GameObject.Destroy(_ImagesToActivePrompts[prompt].gameObject);
        _ImagesToActivePrompts.Remove(prompt);
    }
}
