using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntagonistSounds : MonoBehaviour
{
    public AudioClip EvilGrunt;
    public AudioClip EvilSpell;
    public AudioClip EvilLaugh;
    public AudioClip EvilTeleportAndLaugh;

    public AudioSource SpatialSource;
    public AudioSource GlobalSource;

    public void PlayEvilGrunt()
    {
        SpatialSource.PlayOneShot(EvilGrunt);
    }

    public void PlayEvilSpell()
    {
        SpatialSource.PlayOneShot(EvilSpell);
    }

    public void PlayEvilLaugh()
    {
        SpatialSource.PlayOneShot(EvilLaugh); 
    }

    public void PlayEvilTeleportAndLaughGlobally()
    {
        GlobalSource.PlayOneShot(EvilTeleportAndLaugh);
    }
}
