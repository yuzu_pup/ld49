Shader "Custom/2D/InvertMask"
{
    Properties
    {
        _Color ("Tint Color", Color) = (1,1,1,1)
        _Threshold ("Threshold", Range(0., 1.)) = 0
        _MainTex("Texture", 2D) = "white"{}
    }
    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }

    Cull Off
        Blend SrcAlpha OneMinusSrcAlpha
 GrabPass{
     "_BackgroundTexture"
 }
        Pass
        {
            CGPROGRAM
            #pragma vertex vert_img
            #pragma fragment frag
           
            #include "UnityCG.cginc"
 
            sampler2D _BackgroundTexture;
            float _Threshold;
 
            fixed4 frag (v2f_img i) : SV_Target
            {
                fixed4 col = tex2D(_BackgroundTexture, i.uv);
                col.rgb = abs(_Threshold - col.rgb);
                return col;
            }
            ENDCG
            }
    
    }//endSub
    
}//endShader
